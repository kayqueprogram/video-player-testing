//let video = document.getElementsByClassName('video-screen')[0];

let VideoContainers = document.getElementsByClassName('v-container');


for( let vc of VideoContainers) {

    let src = vc.getAttribute('data-src');
    
    let video = document.createElement('video');
    video.className = 'video';
    video.src = src;
    vc.appendChild(video)

    let Controls = document.createElement('div')
    Controls.className = 'control';

    Controls.appendChild(CreateBtn('../images/Play.png', Play , video));
    Controls.appendChild(CreateBtn('../images/Stop.png', Stop, video));
    Controls.appendChild(CreateBtn('../images/Rewind.png', GoBack, video));
    Controls.appendChild(CreateBtn('../images/Advance.png', Advance, video));

    vc.appendChild(Controls);

    vc.addEventListener('mouseenter', showControl);
    vc.addEventListener('mouseleave', hideControl);
}

function showControl(e) {

    let container = e.target;
    let children = container.children;

    let control = children [ children.length - 1 ];

    control.style.display = 'block';
}

function hideControl(e) {
    
    let container = e.target;
    let children = container.children;

    let control = children [ children.length - 1 ];

    control.style.display = 'none';
}

function CreateBtn( SrcImg, func, video) {

    let btn = document.createElement('img');
    btn.className = 'btn';
    btn.src = SrcImg;
    btn.addEventListener('click', func.bind(video))

    return btn;
}

function Play(e) {
    if(this.paused){
        this.play();
        e.target.src = '../images/Pause.png';
    }else {
        this.pause();
        e.target.src = '../images/Play.png'
    }
};


function Pause() {
    this.pause();
};

function Stop() {
    this.pause();
    this.currentTime = 0;
};

/*
function Inc() {
    this.playbackRate += 0.10;
};


function Dec() {
    this.playbackRate-= 0.10;
};

*/

function GoBack() {
    this.currentTime -= 10;
}

function Advance() {
    this.currentTime += 10;
}

